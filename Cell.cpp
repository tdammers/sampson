#include "Cell.h"

#include <cmath>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cassert>

Cell::Cell():
sample(0), ref_frequency(1.0f),
fixed_pitch(false),
accept_note_off(true),
key_range(0, 128),
velo_inner_range(0, 128),
velo_xfade_range(0, 128),
group(0),
pan(0.0f),
amp(1.0f)
{
}

Cell& Cell::set_sample(Sample* s) {
	sample = s;
	return *this;
}

Cell& Cell::set_key_range(int lo, int hi) {
	key_range.lower = lo;
	key_range.upper = hi;
	return *this;
}

Cell& Cell::set_velo_range(int lo, int hi) {
	return set_velo_range(lo, hi, 0, 0);
}

Cell& Cell::set_velo_range(int lo, int hi, int xfade_lo, int xfade_hi) {
	assert(lo >= 0);
	assert(lo <= 128);
	assert(hi >= 0);
	assert(hi <= 128);
	assert(lo <= hi);
	assert(xfade_lo >= 0);
	assert(xfade_hi >= 0);
	velo_inner_range.lower = lo;
	velo_inner_range.upper = hi;
	velo_xfade_range.lower = lo - xfade_lo;
	velo_xfade_range.upper = hi + xfade_hi;

	return *this;
}

float Cell::get_sample_value(jack_nframes_t sample_pos, float sample_pos_frac, size_t channel) const {
	if (sample)
		return sample->get_sample_value(sample_pos, sample_pos_frac, channel);
	else
		return 0.0f;
}

float Cell::calculate_sample_delta(float freq, jack_nframes_t world_sample_rate) const {
	if (!sample)
		return 0.0f;
	float d = (float)sample->get_sample_rate() / (float)world_sample_rate;
	if (fixed_pitch)
		return d;
	else
		return d * freq / ref_frequency;
}

jack_nframes_t Cell::get_length() const {
	if (sample)
		return sample->get_length();
	else
		return 0;
}

bool Cell::match_note_on(int, int key, int velo) const {
	return key_range.contains(key) && velo_xfade_range.contains(velo);
}

float Cell::velocity_to_amp(int velocity) const {
	if (!velo_xfade_range.contains(velocity))
		return 0.0f;
	if (velo_inner_range.contains(velocity))
		return (float)velocity / 127.0f;
	if (velocity <= velo_inner_range.lower)
		return map_range(
			range<int>(velo_xfade_range.lower, velo_inner_range.lower),
			range<float>(0, (float)velo_inner_range.lower / 127.0),
			velocity);
	else
		return map_range(
			range<int>(velo_xfade_range.upper, velo_inner_range.upper),
			range<float>(0, (float)velo_inner_range.upper / 127.0),
			velocity);
}

float Cell::get_pan_amp(size_t channel) const {
	if (channel & 1) {
		// right channel
		return map_range(range<float>(-1.0f, 1.0f), range<float>(0.0f, 1.0f), pan);
	}
	else {
		// left channel
		return map_range(range<float>(-1.0f, 1.0f), range<float>(1.0f, 0.0f), pan);
	}
}
