#pragma once

#include <jack/jack.h>
#include "Envelope.h"
#include "Sample.h"
#include "util.h"
#include "range.h"

class Cell {
	private:
		EnvelopeParams envelope;
		Sample* sample;
		// The sample's nominal (recorded) frequency. When this frequency is
		// requested, the sample plays at the original pitch.
		float ref_frequency;
		// If set, the sample always plays at the original pitch: it is
		// corrected for sample rate, but no additional re-pitching is
		// performed.
		bool fixed_pitch;
		// If unset, note-off events are ignored (but group-based muting is
		// still performed).
		bool accept_note_off;
		// MIDI key codes to match on; lower bound is inclusive,
		// upper is exclusive
		range<int> key_range;
		// MIDI velocity range to match on; lower bound is inclusive,
		// upper is exclusive. The range is the "inner" velo range, that is, 
		// the range inside which the velocity is not cross-faded.
		range<int> velo_inner_range;
		// MIDI velocity range to match on; lower bound is inclusive,
		// upper is exclusive. This is the total range on which the velocity
		// must match; if the velo_xfade_range is larger than the
		// velo_inner_range, cross-fading is applied so that the velocity at
		// the edges of the velo_xfade range is 0 and fades linearly towards
		// the edges of the velo_inner_range.
		range<int> velo_xfade_range;
		int group;
		// Pan is -1 .. +1, mapping to far left .. far right. 0.0 is dead 
		// center.
		float pan;
		// Overall gain for this cell.
		float amp;
	public:
		Cell();

		Cell& set_sample(Sample*);
		Cell& set_velo_range(int, int);
		Cell& set_velo_range(int, int, int, int);
		Cell& set_key_range(int, int);
		Cell& set_ref_freq(float f, bool fp = false) { ref_frequency = f; fixed_pitch = fp; return *this; }
		Cell& set_ref_note(int n, bool fp = false) { return set_ref_freq(midikey_to_freq(n), fp); }
		Cell& set_fixed_pitch(bool fp = true) { fixed_pitch = fp; return *this; }
		Cell& set_accept_note_off(bool a = true) { accept_note_off = a; return *this; }
		Cell& set_group(int g = 0) { group = g; return *this; }
		Cell& set_pan(float p) { pan = p; return *this; }
		Cell& set_amp(float p) { amp = p; return *this; }

		int get_ref_freq() const { return ref_frequency; }
		Sample* get_sample() const { return sample; }
		const range<int>& get_key_range() const { return key_range; }
		const range<int>& get_velo_range() const { return velo_xfade_range; }
		const range<int>& get_velo_inner_range() const { return velo_inner_range; }
		int get_group() const { return group; }
		float get_pan() const { return pan; }
		float get_pan_amp(size_t channel) const;
		float get_amp() const { return amp; }
		bool get_fixed_pitch() const { return fixed_pitch; }
		bool get_accept_note_off() const { return accept_note_off; }

		float velocity_to_amp(int velocity) const;

		EnvelopeParams& get_envelope_params() { return envelope; }
		const EnvelopeParams& get_envelope_params() const { return envelope; }

		float get_sample_value(jack_nframes_t sample_pos, float sample_pos_frac, size_t channel) const;
		jack_nframes_t get_length() const;
		// calculates the delta by which the sample position should move
		// per frame in the specified sample rate to produce a signal of the
		// specified frequency.
		float calculate_sample_delta(float freq, jack_nframes_t sample_rate) const;
		bool match_note_on(int channel, int key, int velo) const;
};
