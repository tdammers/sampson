#pragma once

#include <jack/jack.h>

class EnvelopeParams {
	public:
		float attack_time;
		float peak_amp;
		float decay_time;
		// sustain_amp <= 0 means there is no sustain phase
		float sustain_amp;
		// release time < 0 means key-off is ignored (indefinite release)
		float release_time;

		EnvelopeParams(float a = 0.0f, float d = 0.0f, float r = -1.0f, float s = 1.0f, float p = 1.0f);
		bool has_sustain() const { return sustain_amp > 0.0f; }
		bool has_release() const { return release_time >= 0.0f; }
		float get_sustain_time() const { return attack_time + decay_time; }
};

class Envelope {
	private:
		const EnvelopeParams* params;
		jack_nframes_t position;
		float wallclock_position;
		bool key_down;
		bool sustain_reached;
		float release_amp;
		float release_time;
		float current_amp;
	public:
		Envelope();
		void start(const EnvelopeParams* _params);
		void release();
		float get_amp() const;
		void next_sample(jack_nframes_t sample_rate);
		bool is_active() const;
};
