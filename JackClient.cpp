#include "JackClient.h"
#include "Sampler.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

JackClient::JackClient(
		Sampler* _sampler,
		size_t _num_audio_ports,
		const char* _client_name):
	sampler(_sampler),
	client(0),
	client_name(_client_name ? strdup(_client_name) : 0),
	midi_ports(0),
	num_midi_ports(1), // sampler currently supports 1 midi input only
	audio_ports(0),
	num_audio_ports(_num_audio_ports),
	initialized(false),
	midi_buffers(0),
	audio_buffers(0)
	{}

JackClient::~JackClient() {
	if (initialized)
		jack_client_close(client);
	free(midi_buffers);
	free(midi_ports);
	free(audio_buffers);
	free(audio_ports);
	free(client_name);
	delete sampler;
}

int JackClient::process_callback(jack_nframes_t nframes, void* arg) {
	JackClient* inst = (JackClient*)arg;
	inst->process(nframes);
	return 0;
}


void JackClient::init() {
	char buffer[64];

	client = jack_client_open(client_name ? client_name : "sampson", JackNullOption, NULL);

	midi_ports = (jack_port_t**)malloc(sizeof(jack_port_t*) * num_midi_ports);
	midi_buffers = (void**)malloc(sizeof(void*) * num_midi_ports);
	for (size_t i = 0; i < num_midi_ports; ++i) {
		sprintf(buffer, "midi_in_%i", (int)i);
		midi_ports[i] = jack_port_register(
			client,
			buffer,
			JACK_DEFAULT_MIDI_TYPE,
			
			JackPortIsInput | JackPortIsTerminal,
			0);
	}

	audio_ports = (jack_port_t**)malloc(sizeof(jack_port_t*) * num_audio_ports);
	audio_buffers = (void**)malloc(sizeof(void*) * num_audio_ports);
	for (size_t i = 0; i < num_audio_ports; ++i) {
		sprintf(buffer, "audio_%i", (int)i);
		audio_ports[i] = jack_port_register(
			client,
			buffer,
			JACK_DEFAULT_AUDIO_TYPE,
			JackPortIsOutput | JackPortIsTerminal,
			0);
	}

	jack_set_process_callback(client, process_callback, (void*)this);

	jack_activate(client);

	initialized = true;
}

void JackClient::process(jack_nframes_t nframes) {
	for (size_t i = 0; i < num_midi_ports; ++i) {
		midi_buffers[i] = jack_port_get_buffer(midi_ports[i], nframes);
	}
	for (size_t i = 0; i < num_audio_ports; ++i) {
		audio_buffers[i] = jack_port_get_buffer(audio_ports[i], nframes);
	}
	sampler->process(*this, nframes);
}

jack_nframes_t JackClient::get_sample_rate() const {
	if (initialized)
		return jack_get_sample_rate(client);
	else
		return 0;
}
