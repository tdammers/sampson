#pragma once

#include <jack/jack.h>

class Sampler;

class JackClient {
	private:
		Sampler* sampler;
		jack_client_t* client;
		char* client_name;
		jack_port_t** midi_ports;
		size_t num_midi_ports;
		jack_port_t** audio_ports;
		size_t num_audio_ports;
		bool initialized;
		bool started;
		void** midi_buffers;
		void** audio_buffers;

		static int process_callback(jack_nframes_t nframes, void* arg);
	
	public:
		JackClient(Sampler*, size_t, const char* client_name = 0);
		~JackClient();
		void init();
		void process(jack_nframes_t nframes);

		size_t get_num_audio_ports() const { return num_audio_ports; }
		size_t get_num_midi_ports() const { return num_midi_ports; }
		void** get_audio_buffers() { return audio_buffers; }
		void** get_midi_buffers() { return midi_buffers; }
		jack_nframes_t get_sample_rate() const;
};

