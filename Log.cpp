#include "Log.h"

#include <iostream>

Log::Log(size_t _buffer_size):
buffer_size(_buffer_size),
buffer((char*)malloc(_buffer_size)),
read_pos(0),
write_pos(0),
lines_written(0),
lines_read(0)
{
}

Log::~Log() {
	free(buffer);
}

void Log::putch(char c) {
	buffer[write_pos] = c;
	++write_pos;
	write_pos %= buffer_size;
	if (c == '\n')
		++lines_written;
}

void Log::putstr(const char* s) {
	const char* ss = s;
	while (*ss) {
		putch(*ss);
		++ss;
	}
}

void Log::putln(const char* s) {
	putstr(s);
	putch('\n');
}

void Log::flush_line() {
	while (read_pos != write_pos) {
		char c = buffer[read_pos];
		std::clog << c;
		++read_pos;
		read_pos %= buffer_size;
		if (c == '\n') {
			--lines_read;
			break;
		}
	}
}

void Log::flush_lines() {
	while (lines_read < lines_written)
		flush_line();
}

void Log::flush() {
	while (read_pos != write_pos) {
		char c = buffer[read_pos];
		std::clog << c;
		++read_pos;
		read_pos %= buffer_size;
		if (c == '\n') {
			--lines_read;
		}
	}
}

Log& Log::get_def_inst() {
	static Log deflog(1024);
	return deflog;
}
