#pragma once

#include <cstdlib>

class Log {
	private:
		size_t buffer_size;
		char* buffer;
		size_t read_pos;
		size_t write_pos;
		size_t lines_written;
		size_t lines_read;
	public:
		Log(size_t _buffer_size);
		~Log();
		void flush();
		void flush_line();
		void flush_lines();
		void putch(char c);
		void putln(const char* s);
		void putstr(const char* s);
		size_t lines_available() const { return lines_written - lines_read; }

		static Log& get_def_inst();
};
