#pragma once

#include <jack/jack.h>

class Sample {
	private:
		jack_nframes_t sample_rate;
		jack_nframes_t nframes;
		float* sample_data;
		size_t nchannels;
	public:
		Sample();
		~Sample();
		float get_sample_value(jack_nframes_t sample_pos, float sample_pos_frac, size_t channel) const;
		void get_sample_values_raw(float *buffer, jack_nframes_t sample_pos, size_t channel) const;
		float get_sample_rate() const { return sample_rate; }
		void load_sample(jack_nframes_t _nframes, float* _data, jack_nframes_t _sample_rate, size_t _nchannels);
		void load_sample_file(const char* filename);
		jack_nframes_t get_length() const { return nframes; }
};
