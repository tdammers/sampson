#pragma once

#include <jack/jack.h>
#include <jack/midiport.h>
#include <string>
#include <map>
#include <vector>
#include <iostream>

class JackClient;
class Cell;
class Voice;
class Sample;

class Sampler {
	private:
		size_t num_voices;
		Voice* voices;
		std::vector<Cell*> cells;
		std::map<std::string, Sample*> samples;

		void processPartially(jack_nframes_t sample_rate, size_t num_audio_buffers, float** audio_buffers, jack_nframes_t frame0, jack_nframes_t nframes);
		void processNoteOn(const jack_midi_event_t&);
		void processNoteOff(const jack_midi_event_t&);
		size_t findFreeVoice() const;
		void noteOffByGroup(int group);
	public:
		Sampler(size_t _num_voices = 64);
		~Sampler();
		void process(JackClient&, jack_nframes_t);
		void processMidiEvent(const jack_midi_event_t&);
		Sample* load_sample(const std::string&);
		Sampler& addCell(Cell*);
		void dump_state(std::ostream&) const;
};
