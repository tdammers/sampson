#include "Voice.h"
#include "JackClient.h"
#include "Cell.h"
#include "util.h"
#include <cmath>

Voice::Voice():
cell(0), channel(0), key(0), velocity(0), key_down(false),
sample_pos(0), sample_pos_frac(0.0f), envelope()
{}

void Voice::noteOn(int _channel, int _key, int _velocity, const Cell* _cell) {
	channel = _channel;
	key = _key;
	velocity = _velocity;
	cell = _cell;
	key_down = true;
	sample_pos = 0;
	sample_pos_frac = 0.0f;
	// magic: mark delta-phi as "not calculated yet"
	delta_phi = -1.0f;
	if (cell)
		envelope.start(&cell->get_envelope_params());
}

void Voice::noteOff(bool force) {
	if (force || cell->get_accept_note_off()) {
		key_down = false;
		envelope.release();
	}
}

void Voice::process(jack_nframes_t sample_rate, size_t num_audio_buffers, float** audio_buffers, jack_nframes_t frame0, jack_nframes_t nframes) {
	if (num_audio_buffers == 0)
		return;
	if (!isActive())
		return;
	float* buffers[2] = {
		audio_buffers[0] + frame0,
		audio_buffers[1] + frame0
	};
	float envelope_amp = 0.0f;
	float main_amp = cell->get_amp();
	float sample_value = 0.0f;
	float velocity_amp = cell->velocity_to_amp(velocity);
	float pan_amp[2];
	float amp;
	int full_samples;

	if (buffered_sample_rate != sample_rate || delta_phi <= 0.0f) {
		delta_phi = getDeltaPhi(sample_rate);
	}

	for (size_t c = 0; c < 2; ++c)
		pan_amp[c] = cell->get_pan_amp(c);
	for (jack_nframes_t f = 0; f < nframes; ++f) {
		envelope_amp = envelope.get_amp();
		amp = envelope_amp * main_amp * velocity_amp;
		for (size_t c = 0; c < 2; ++c) {
			sample_value = cell->get_sample_value(sample_pos, sample_pos_frac, c);
			*buffers[c] += sample_value * amp * pan_amp[c];
			++buffers[c];
		}
		sample_pos_frac += delta_phi;
		full_samples = floor(sample_pos_frac);
		sample_pos += (int)full_samples;
		sample_pos_frac -= full_samples;
		envelope.next_sample(sample_rate);
	}
}

float Voice::getFrequency() const {
	return midikey_to_freq(key);
}

float Voice::getDeltaPhi(jack_nframes_t sample_rate) const {
	if (cell)
		return cell->calculate_sample_delta(getFrequency(), sample_rate);
	else
		return 0.0f;
}

bool Voice::isActive() const {
	if (!cell)
		return false;
	if (sample_pos >= cell->get_length())
		return false;
	return envelope.is_active();
}

int Voice::getGroup() const {
	if (cell)
		return cell->get_group();
	else
		return 0;
}
