#pragma once

#include <iostream>
#include <string>

class Sampler;

Sampler& loadPatch(Sampler&, const char*);
Sampler& loadPatch(Sampler&, std::istream&);
