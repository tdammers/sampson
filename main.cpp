#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include "load_patch.h"
#include "JackClient.h"
#include "Sampler.h"
#include "Log.h"
#include <iostream>
#include <unistd.h>
#include <getopt.h>

int sig = 0;

void handle_signal(int s) { sig = s; }

struct Opts {
	const char* client_name;
	const char* patch_filename;
	Opts(): client_name(0), patch_filename(0) { }
};

void print_help(char* progname) {
	printf("Usage: %s [-c NAME] [KITFILE]\n", progname);
	printf("\n");
	printf("       --help             Display this help\n");
	printf("-c     --client-name=NAME Set Jack client name to NAME\n");
}

void parse_args(Opts& opts, int argc, char* argv[]) {
	int option_index = 1;
	static struct option long_options[] = {
		{"client-name", required_argument, 0, 'c'},
		{"help", no_argument, 0, 0 },
		{0, 0, 0, 0 }
	};
	while (true) {
		int c = getopt_long(argc, argv, "c:", long_options, &option_index);
		if (c == -1)
			break;
		switch (c) {
			case 0:
				switch (option_index) {
					case 1:
						print_help(argv[0]);
						exit(0);
				}
				break;
			case 'c':
				opts.client_name = optarg;
				break;
		}
	}
	if (optind < argc) {
		opts.patch_filename = argv[optind++];
	}
}

int main(int argc, char* argv[]) {
	Opts opts;
	parse_args(opts, argc, argv);
	Log& log = Log::get_def_inst();

	Sampler* sampler = new Sampler();
	if (opts.patch_filename)
		loadPatch(*sampler, opts.patch_filename);
	sampler->dump_state(std::clog);
	JackClient client(sampler, 2, opts.client_name);
	signal(SIGTERM, handle_signal);

	client.init();
	printf("Sample rate: %i\n", (int)client.get_sample_rate());
	while (!sig) {
		log.flush();
		usleep(100000);
	}

	return 0;
}
