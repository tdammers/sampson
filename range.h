#pragma once

template<typename T>
class range {
	public:
		T lower;
		T upper;
		range(const T& _lower = 0, const T& _upper = 0):
			lower(_lower), upper(_upper) {}
		range(const range& rhs):
			lower(rhs.lower), upper(rhs.upper) {}
		range& operator=(const range& rhs) {
			lower = rhs.lower();
			upper = rhs.upper();
		}

		bool operator==(const range& rhs) const {
			return lower == rhs.lower && upper == rhs.upper;
		}

		bool operator!=(const range& rhs) const { return !(*this == rhs); }

		T size() const { return upper - lower; }
		bool contains(const T& val) const { return (val >= lower) && (val < upper); }
};

template<typename T, typename P>
T map_range(const range<P>& src, const range<T>& dst, const P& p) {
	return (T)(p - src.lower) * (dst.upper - dst.lower) / (T)(src.upper - src.lower) + dst.lower;
}
