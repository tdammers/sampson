#!/usr/bin/env bash
# KIT=kits/testing/testing.kit
# KIT=~/samplib/asian/sampson-kits/di_zi_vib.kit
KIT=kits/hydrogen/YamahaVintageKit/drumkit.kit
cd $(dirname $0)
./sampson "$KIT" &
jack-keyboard &
sleep 3
jack_connect sampson:audio_0 system:playback_1
jack_connect sampson:audio_1 system:playback_2
jack_connect jack-keyboard:midi_out sampson:midi_in_0
read
killall jack-keyboard
killall sampson
