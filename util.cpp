#include "util.h"
#include <cmath>
#include <cstring>

float clamp_to_range(float value, float lo, float hi) {
	if (value > hi)
		return hi;
	if (value < lo)
		return lo;
	return value;
}

float midikey_to_freq(float key) {
	// middle a, 440 Hz, is MIDI key 69
	return 440.0f * pow(2.0f, (key - 69.0f) / 12.0f);
}

float db_to_amp(float db) {
	return 1.0f * pow(0.5f, db / 10.0f);
}

int is_numeric(int c) {
	return (c >= '0' && c <= '9');
}

int is_note_basename(int c) {
	return (c >= 'a' && c <= 'g') || (c >= 'A' && c <= 'G');
}

int is_accidental(int c) {
	return (c == '#' || c == 'b');
}

int basename_to_midikey(int c) {
	switch (c) {
		case 'C':
		case 'c': return 0;
		case 'D':
		case 'd': return 2;
		case 'E':
		case 'e': return 4;
		case 'F':
		case 'f': return 5;
		case 'G':
		case 'g': return 7;
		case 'A':
		case 'a': return 9;
		case 'B':
		case 'b': return 11;
		default: return -1;
	}
}

int note_to_midikey(int base, int acc, int oct) {
	return basename_to_midikey(base) + acc + 12 * oct;
}

