#pragma once

float clamp_to_range(float value, float lo, float hi);
float midikey_to_freq(float k);
float db_to_amp(float db);

int is_numeric(int c);
int is_note_basename(int c);
int is_accidental(int c);
int basename_to_midikey(int c);
int note_to_midikey(int base, int acc, int oct);
