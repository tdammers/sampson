<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
    <xsl:output method="text" />
    
    <xsl:template match="/drumkit_info">
		<xsl:text>;;; </xsl:text>
		<xsl:value-of select="name" />
		<xsl:call-template name="endl" />

		<xsl:text>;;; </xsl:text>
		<xsl:value-of select="translate(info, '\n', ' ')" />
		<xsl:call-template name="endl" />

		<xsl:text>;;; Author: </xsl:text>
		<xsl:value-of select="translate(author, '\n', ' ')" />
		<xsl:call-template name="endl" />

		<xsl:apply-templates select="instrumentList/instrument" />
    </xsl:template>

	<xsl:template match="instrument[layer]">
		<xsl:text>;;; </xsl:text>
		<xsl:value-of select="name" />
		<xsl:call-template name="endl" />
		<xsl:apply-templates select="layer" />
	</xsl:template>

	<xsl:template match="instrument[filename]">
		<xsl:text><![CDATA[file ]]></xsl:text>
		<xsl:value-of select="filename" />
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[fixedpitch]]></xsl:text>
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[keyrange ]]></xsl:text>
		<xsl:value-of select="id + 36" />
		<xsl:text><![CDATA[ ]]></xsl:text>
		<xsl:value-of select="id + 36" />
		<xsl:call-template name="endl" />

		<xsl:if test="volume">
			<xsl:text><![CDATA[amp ]]></xsl:text>
			<xsl:value-of select="volume" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="Attack">
			<xsl:text><![CDATA[att ]]></xsl:text>
			<xsl:value-of select="Attack" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="Decay" >
			<xsl:text><![CDATA[dec ]]></xsl:text>
			<xsl:value-of select="Decay" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="Sustain" >
			<xsl:text><![CDATA[sus ]]></xsl:text>
			<xsl:value-of select="Sustain" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="Release and Release &lt; 60" >
			<xsl:text><![CDATA[rel ]]></xsl:text>
			<xsl:value-of select="Release" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:text><![CDATA[--]]></xsl:text>
		<xsl:call-template name="endl" />
		<xsl:call-template name="endl" />
	</xsl:template>

	<xsl:template match="layer">
		<xsl:text><![CDATA[file ]]></xsl:text>
		<xsl:value-of select="filename" />
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[fixedpitch]]></xsl:text>
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[keyrange ]]></xsl:text>
		<xsl:value-of select="../id + 36" />
		<xsl:text><![CDATA[ ]]></xsl:text>
		<xsl:value-of select="../id + 36" />
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[velorange ]]></xsl:text>
		<xsl:value-of select="round(min * 127 - 0.5)" />
		<xsl:text><![CDATA[ ]]></xsl:text>
		<xsl:value-of select="round(max * 127 + 0.5)" />
		<xsl:call-template name="endl" />

		<xsl:text><![CDATA[amp ]]></xsl:text>
		<xsl:value-of select="gain * ../volume" />
		<xsl:call-template name="endl" />

		<xsl:if test="../Attack">
			<xsl:text><![CDATA[att ]]></xsl:text>
			<xsl:value-of select="../Attack" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="../Decay" >
			<xsl:text><![CDATA[dec ]]></xsl:text>
			<xsl:value-of select="../Decay" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="../Sustain" >
			<xsl:text><![CDATA[sus ]]></xsl:text>
			<xsl:value-of select="../Sustain" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:if test="../Release and ../Release &lt; 60" >
			<xsl:text><![CDATA[rel ]]></xsl:text>
			<xsl:value-of select="../Release" />
			<xsl:call-template name="endl" />
		</xsl:if>

		<xsl:text><![CDATA[--]]></xsl:text>
		<xsl:call-template name="endl" />
		<xsl:call-template name="endl" />
	</xsl:template>

	<xsl:template match="*|@*">
	</xsl:template>
    
    <xsl:template name="endl">
        <xsl:text><![CDATA[
]]></xsl:text>
    </xsl:template>
</xsl:stylesheet>
